var teeshirts = angular.module('teeshirtApp',[
    'ngRoute',
    'ngStorage',
    'Shirts',
    'Auth',
    'Dashboard',
    'homepageServices',
    'ezfb'
    ]);

teeshirts.config(function (ezfbProvider) {
  /**
   * Basic setup
   *
   * https://github.com/pc035860/angular-easyfb#configuration
   */
  ezfbProvider.setInitParams({
    appId: '1466820216890548'
  });  
}); 
teeshirts.controller('mainController', function($scope,$window, $localStorage, $timeout, AuthServices,ezfb){
    
    $scope.isLogin          = false;
    $scope.isReady          = false;
    $scope.footer           = true;
    $scope.header           = true;
    $scope.shirt_selected   = 0;
    $scope.account          = {};
    $scope.body_padding     = "50px;";
   
   
    AuthServices.getAccount(function(account){
        $scope.isReady          =   true;
       
        if(!account.error){
            $scope.account      = account;
            $scope.isLogin      = true;
        }
    });  
     
     

    $scope.templates = {
        'header'        : 'app/templates/header.html?_ct='+new Date().getTime(),
        'footer'        : 'app/templates/footer.html?_ct='+new Date().getTime()
    };


    $scope.showShirtDetails = function(id,name){
        $scope.shirt_selected = id;
        $window.location.hash = "#/shirt/"+name;
    };

    $scope.logout =   function(){        
        $localStorage.$reset();
        $timeout(function(){
            $window.location.reload();
        },100);
    };
    $scope.fblogin = function() {
        ezfb.login(null, {
          scope: 'email,user_likes'
        }).then(function (res) {
             if(res.status!="unknown")
             {
                 console.log(res);
                 console.log( ezfb.api('/me'));
                 console.log("logged in...");

                 $scope.isLogin = true;
                $window.location.hash = "#/";
                 //save info here
             }
         });
    };

});
