var auth = angular.module('Auth',[]);

auth.config(function($routeProvider){

    $routeProvider.when('/login',{
        templateUrl : 'app/modules/auth/login.html?_ct='+new Date().getTime(),
        controller  : 'loginController'
    });

});

auth.controller('loginController', function($scope, $rootScope,$localStorage, $window, $timeout, AuthServices){
    
    $scope.user = {};
    $timeout(function(){
        if($scope.$parent.isLogin){
            $window.location.hash = "#/dashboard";        
        }
    },100);
    
    $scope.$parent.header = false;
    $scope.$parent.footer = false;
    $scope.$parent.body_padding     = "0px;";
    
    var isValidEmail  =   function(email){
        if(email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }else{
            return false;
        }
    };
    

    $scope.login = function(user){
        user.error = {};
        
        if(!isValidEmail(user.email))
            user.error.email = " not valid";
        if(!user.password)
            user.error.password = " required";
        
        if($.isEmptyObject(user.error)){
            
            AuthServices.Login(user.email,user.password,function(error,token){
                
                if(!error){
                    $localStorage.token =   token;         
                    $timeout(function(){
                        $window.location.reload();
                    },100);
                }else{
                   
                }

            });
        }
       
    };

});