var dashboard = angular.module('Dashboard',[]);

dashboard.config(function($routeProvider){

    $routeProvider.when('/dashboard',{
        templateUrl : 'app/modules/dashboard/index.html?_ct='+new Date().getTime(),
        controller  : 'dashboardInit'
    });


    
});

dashboard.controller('dashboardInit', function($scope, $window){

    if(!$scope.$parent.isLogin){
        $window.location.hash = "#/";       
    }
    
    $scope.$parent.header       = true;
    $scope.$parent.footer       = false;
    $scope.profile = {};

    $scope.templates = {
        "sidebar"   : 'app/modules/dashboard/partials/sidebar.html?_ct='+new Date().getTime(),
        "content"   : 'app/modules/dashboard/partials/posts.html?_ct='+new Date().getTime()
    };

    $scope.changeTab = function(tab){
        $scope.templates.content = 'app/modules/dashboard/partials/'+tab+'.html?_ct='+new Date().getTime()
    };
    
    $scope.updateProfile = function(profile){
      console.log(profile);  
    };


});