var shirts = angular.module('Shirts', ['ezfb']);

shirts.config(function ($routeProvider) {

    $routeProvider.when('/', {
        templateUrl: 'app/modules/shirts/index.html?_ct='+new Date().getTime(),
        controller: 'ShirtsListing'
    });

    $routeProvider.when('/shirt/:name', {
        templateUrl: 'app/modules/shirts/details.html?_ct='+new Date().getTime(),
        controller: 'ShirtsDetails'
    });

    $routeProvider.when('/post', {
        templateUrl: 'app/modules/shirts/post.html?_ct='+new Date().getTime(),
        controller: 'PostShirt'
    });

    $routeProvider.when('/shirt/edit/:name', {
        templateUrl: 'app/modules/shirts/edit.html?_ct='+new Date().getTime(),
        controller: 'EditShirt'
    });

});

shirts.controller('ShirtsListing', function ($scope, ShirtServices) {

    $scope.$parent.header = true;
    $scope.$parent.footer = true;
    $scope.$parent.shirt_selected = 0;
    $scope.active_filter = 'all';
    $scope.shirtlisting = {};
    $scope.sfilter = {};
    $scope.limit = 20;
    $scope.selected_filter_type = "Choose Type";
    $scope.selected_filter_category = "Choose Category";

    $scope.filters = {
        "page": 1,
        "limit": 20,
        "q": '',
        "category": '',
        "type": '',
        "status": ''
    };
    
//    applyFilter();

    $scope.templates = {
        'filters': 'app/modules/shirts/partials/filters.html?_ct=' + new Date().getTime(),
        'shirt': 'app/modules/shirts/partials/shirt.html?_ct=' + new Date().getTime(),
        'footerlinks': 'app/modules/shirts/partials/footerlinks.html?_ct=' + new Date().getTime()
    };



    ShirtServices.Shirts($scope.filters, function (shirtlisting) {
        $scope.shirtlisting = shirtlisting;
    });


    ShirtServices.Filters(function (sfilter) {
        $scope.sfilter = sfilter;
    });


    $scope.shirtFilter = function (filter) {

        $scope.active_filter = filter;
        $scope.filters.page = 1;
        $scope.filters.status = '';
        $scope.filters.limit = 20;
        $scope.filters.q = '';
  
        if (filter === 'latest') {
            $scope.filters.orderby = "approved_date";
        }

        if (filter === 'male') {
            $scope.filters.gender = "male";
        }

        if (filter === 'female') {
            $scope.filters.gender = "female";
        }
        applyFilter();
       
    };

    $scope.changeFilterType = function (type) {
        $scope.selected_filter_type = type;
        $scope.filters.type = type;
       
        applyFilter();
    };

    $scope.changeFilterCategory = function (category) {
        $scope.selected_filter_category = category;
        $scope.filters.category = category;
       
        applyFilter();
    };
    
    var applyFilter = function(){
        
        ShirtServices.Shirts($scope.filters, function (shirtlisting) {
            $scope.shirtlisting = shirtlisting;
        });
    };


});


shirts.controller('ShirtsDetails', function ($scope, ShirtServices, ezfb) {

    var container_local = angular.element('.full-img-wrapper').width();
    var percentage = container_local * .037;
    $scope.image_wrapper = container_local - percentage;
    $scope.$parent.header = true;
    $scope.$parent.footer = true;
    $scope.limit = 4;

    $scope.templates = {
        'shirt': 'app/modules/shirts/partials/shirt.html?_ct=' + new Date().getTime(),
        'details_sidebar': 'app/modules/shirts/partials/details-sidebar.html?_ct=' + new Date().getTime()
    };

    $scope.arrays = [1, 2, 3, 4, 5];

    $scope.details = {};
    $scope.link = document.URL;
    ShirtServices.Shirt($scope.$parent.shirt_selected, function (details) {
        $scope.details = details;
    });
    
    ShirtServices.Shirts(function (shirtlisting) {
        $scope.shirtlisting = shirtlisting;
    });

    $scope.moreDetails = function () {

        angular.element(".dcollapse").slideToggle();
    };

    $scope.loveShirt = function (id) {
        alert('iloveit');
    };

    $scope.shareShirt = function (id) {
        var no = 1,
        callback = function (res) {
          if(res!=null && no==1)
          {
             alert(id);
            ShirtServices.ShirtShare(api+"/love/",id,function(){
                
            });
            no++;
          }
        };
          ezfb.ui(
          {
            method: 'feed',
            name: $scope.details.shirt_name,
            link: $scope.link,
            description: $scope.details.shirt_description
          },
          callback
        )
        .then(callback);
    };


});

shirts.controller('PostShirt', function ($scope, $localStorage, ShirtServices) {

    $scope.tee = {};
    $scope.brand = {};
    $scope.tee.error = {};
    $scope.colors = {};
    $scope.details = {};
    $scope.$parent.header = true;
    $scope.$parent.footer = true;
    $scope.template = 'app/modules/shirts/post/step-1.html?_ct=' + new Date().getTime();
    $scope.step = 1;
    $scope.other_size = false;
    $scope.selected_colors = [];
    $scope.selected_sizes = [];
    $scope.other_size_selected = [];
    $scope.gender = [];
    $scope.picker  = false;
    $scope.phone_error = false;

    
    ShirtServices.Colors(function (colors) {
        $scope.colors = colors;
    });

    ShirtServices.Types(function (details) {
        $scope.details = details;
    });
    
    $scope.changeType = function (id, name) {
        $scope.tee.selected_type = name;
        $scope.tee.type = id;
    };


    $scope.changeOccasion = function (id, name) {
        $scope.tee.selected_occasion = name;
        $scope.tee.occasion = id;
    };


    $scope.changeFabric = function (id, name) {
        $scope.tee.selected_fabric = name;
        $scope.tee.fabric = id;
    };
    
    $scope.addColor = function (color) {
        if ($.inArray(color, $scope.selected_colors) == -1) {
            $scope.selected_colors.push(color);
            $scope.tee.colors = $scope.selected_colors;
        }
    };    
    
    $scope.removeColor = function (color) {
        $scope.selected_colors.splice($.inArray(color, $scope.selected_colors), 1);
        $scope.tee.colors = $scope.selected_colors;
    };

    $scope.addSize = function(){
        var val = angular.element(".other_sizes").val();
     
        if ($.inArray(val, $scope.other_size_selected) == -1) {
            $scope.other_size_selected.push(val);
        }
        
        $scope.tee.other_size = $scope.other_size_selected;
    };
    
    $scope.removeSize = function(val){
        $scope.other_size_selected.splice($.inArray(val, $scope.other_size_selected), 1);
        $scope.tee.other_size = $scope.other_size_selected;
    };
    
    $scope.selectSize = function(size){
        if ($.inArray(size, $scope.selected_sizes) == -1) {
            $scope.selected_sizes.push(size);
            
        }else{
            $scope.selected_sizes.splice($.inArray(size, $scope.selected_sizes), 1);
        }
        $scope.tee.size = $scope.selected_sizes;
        
    };
    
    $scope.addGender = function(gender){
        if ($.inArray(gender, $scope.gender) == -1) {
            $scope.gender.push(gender);            
        }else{
            $scope.gender.splice($.inArray(gender, $scope.gender), 1);
        }
        $scope.tee.gender = $scope.gender;
    };
    
    $scope.showSize = function () {
        $scope.other_size_selected = [];
        $scope.tee.other_size = [];
        if ($scope.other_size) {
            $scope.other_size = false;
        } else {
            $scope.other_size = true;
        }
    };
    
    $scope.showPicker = function(){        
        $scope.picker = true;
        setTimeout(function () { 
            angular.element(".cpicker").minicolors();
        }, 10);       
    };
    
    $scope.addNewColor = function(){
        var new_color = angular.element(".cpicker").val();        
        if ($.inArray(new_color, $scope.selected_colors) == -1) {
            $scope.selected_colors.push(new_color);
            $scope.tee.colors = $scope.selected_colors;
        }
    };
    
    $scope.photo = "";
    
    $scope.changePostStep = function (tee, page) {
        tee.error = {};
        if (!tee.image)
            tee.error.image = " required";
        if (!tee.name)
            tee.error.name = " required";
        if (!tee.type)
            tee.error.type = " required";            
        if ((!tee.size || tee.size.length < 1) && (!tee.other_size || tee.other_size.length < 1))
            tee.error.size = " required";
        if (!tee.gender || tee.gender.length < 1)
            tee.error.gender = " required";
        if (!tee.colors || tee.colors.length < 1)
            tee.error.color = " required";
        if ($scope.photo)
            tee.error.photo = $scope.photo;        
        
        if (!tee.price){
            tee.error.price = " required";
        } else {
            if (isNaN(tee.price))
                tee.error.price = " should be a number";
        }
          
        if ($.isEmptyObject(tee.error)) {

            if ($scope.$parent.isLogin == true) {                
                ShirtServices.Post(tee, {}, function (post) {
                    if (!post.error) {
                        $scope.step = 3;
                        $scope.template = 'app/modules/shirts/post/step-3.html?_ct=' + new Date().getTime();
                    }
                });

            } else {
                $scope.step = 2;
                $scope.template = 'app/modules/shirts/post/step-2.html?_ct=' + new Date().getTime();
            }
        } 
    };
    $scope.phones = {};
    
    $scope.addPhone = function(){
        var network = angular.element(".phone_network").val();
        var number  = angular.element(".phone_number").val();
        $scope.phone_error = false;
        if(isNaN(number)){
            $scope.phone_error = true;
            return;
        }
        
        if ($.inArray(network, $scope.phones) == -1) {
            $scope.phones[network] = {"network": network,"number": number};
        }
        
        $scope.brand.phone = $scope.phones;
    };
    
    $scope.removePhone = function(network){
        delete $scope.phones[network];
        $scope.brand.phone = $scope.phones;
    };
    $scope.selected_location = "";
    $scope.addLocation = function(location){
        $scope.selected_location = location;
        $scope.brand.location = location;
    };  
    
    
    var isValidEmail  =   function(email){
        if(email){
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }else{
            return false;
        }
    };

    $scope.changePostStep2 = function (brand) {

        brand.error = {};
        if (!brand.name)
            brand.error.name = " required";
        if (!brand.location)
            brand.error.location = " required";
        if (!brand.phone || $.isEmptyObject(brand.phone))
            brand.error.phone = " required";
        
        if (!brand.email){
            brand.error.email = " required";
        }else{
            if(!isValidEmail(brand.email)){
                brand.error.email = " not valid";
            }
        }
        
        if ($.isEmptyObject(brand.error)) {
            ShirtServices.Post($scope.tee, brand, function (post) {               
                if (!post.error) {
                    $scope.step = 3;
                    $scope.template = 'app/modules/shirts/post/step-3.html?_ct=' + new Date().getTime();
                }
            });
        }
    };

    $scope.addPhoto = function () {

        setTimeout(function () {
            angular.element('.tee-upload').click();
            angular.element('.tee-upload').change(function (e) {
                var input = e.target;
                loadImage(input.files);
            });
        }, 10);
    };

    var loadImage = function (file) {
       
        file = file[0];
        var reader = new FileReader();
        if (file) {
            
            reader.onload = function () {
                var image = new Image();
                image.src = reader.result;
                var data = reader.result;
                
                if(image.width != 800 && image.height != 400){                    
                    $scope.photo = " Image dimension should be 800 X 400";                   
                }
                
                data = data.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                $scope.tee.image = data;
                var img = $("<img/>").attr('class', 'img-responsive').attr("src", "data:image/gif;base64," + data);
                angular.element('.tee_preview').html(img);
            };

            reader.readAsDataURL(file);
        }
    };
    
});

shirts.controller('EditShirt', function ($scope, $window, ShirtServices) {
    
    $scope.colors = {};
    $scope.sdetails = {};
    $scope.details = {};
    $scope.tee = {};    
    $scope.tee.error = {};
    $scope.selected_sizes = [];
    $scope.other_size_selected = [];
    $scope.tee.other_size = [];
    $scope.other_size = false;
    $scope.gender = [];
    $scope.selected_colors = [];
    $scope.photo = "";
    
    $scope.$parent.header = true;
    $scope.$parent.footer = true;
    
//    if (!$scope.$parent.isLogin) {
//        $window.location.hash = "#/";
//    }
//    
    ShirtServices.Colors(function (colors) {
        $scope.colors = colors;
    });

    ShirtServices.Types(function (details) {
        $scope.sdetails = details;
        
    });
    
    


    ShirtServices.Shirt(1, function (details) {       
        $scope.details = details;
        $scope.details.sizes = Object.keys($scope.details.sizes).map(function (key) {return $scope.details.sizes[key]});
        $scope.details.other_sizes = Object.keys($scope.details.other_sizes).map(function (key) {return $scope.details.other_sizes[key]});
        $scope.details.shirt_gender = Object.keys($scope.details.shirt_gender).map(function (key) {return $scope.details.shirt_gender[key]});
        $scope.details.color = Object.keys($scope.details.color).map(function (key) {return $scope.details.color[key]});
        $scope.gender = $scope.details.shirt_gender;
        $scope.selected_sizes = $scope.details.sizes;
        $scope.other_size_selected = $scope.details.other_sizes;
        $scope.selected_colors = $scope.details.color;
        $scope.tee.price = $scope.details.price;
        $scope.tee.shirt_name = $scope.details.shirt_name;
    });
    
    $scope.changeType = function (id, name) {
        $scope.details.type = name;
        $scope.tee.type = id;
        
    };
    
    $scope.changeOccasion = function (id, name) {
        $scope.details.occasion = name;
        $scope.tee.occasion = id;        
    };
    
    $scope.changeFabric = function (id, name) {
        $scope.details.fabric = name;
        $scope.tee.fabric = id;        
    };
    
    
    $scope.selectSize = function(size){        
        if ($.inArray(size, $scope.selected_sizes) == -1) {
            $scope.selected_sizes.push(size);            
        }else{
            $scope.selected_sizes.splice($.inArray(size, $scope.selected_sizes), 1);
        }
        $scope.tee.size = $scope.selected_sizes;
      
    };
    
    
    $scope.showSize = function () {
        if ($scope.other_size) {
            $scope.other_size = false;
        } else {
            $scope.other_size = true;
        }
    };
    
    $scope.addSize = function(){
        var val = angular.element(".other_sizes").val();        
        if ($.inArray(parseInt(val), $scope.other_size_selected) == -1) {
            $scope.other_size_selected.push(parseInt(val));
        }        
        $scope.tee.other_size = $scope.other_size_selected;
        
    };
    
    $scope.removeSize = function(val){
        $scope.other_size_selected.splice($.inArray(val, $scope.other_size_selected), 1);
        $scope.tee.other_size = $scope.other_size_selected;
    };
    
    $scope.addGender = function(gender){
        if ($.inArray(gender, $scope.gender) == -1) {
            $scope.gender.push(gender);            
        }else{
            $scope.gender.splice($.inArray(gender, $scope.gender), 1);
        }        
        $scope.tee.gender = $scope.gender;        
    };
    
    
    
    $scope.addColor = function (color) {
        if ($.inArray(color, $scope.selected_colors) == -1) {
            $scope.selected_colors.push(color);
            $scope.tee.colors = $scope.selected_colors;
        }
    };

    $scope.removeColor = function (color) {
        $scope.selected_colors.splice($.inArray(color, $scope.selected_colors), 1);
        $scope.tee.colors = $scope.selected_colors;
       
    };
    
    $scope.showPicker = function(){        
        $scope.picker = true;
        setTimeout(function () { 
            angular.element(".cpicker").minicolors();
        }, 10);       
    };
    
    $scope.addNewColor = function(){
        var new_color = angular.element(".cpicker").val();        
        if ($.inArray(new_color, $scope.selected_colors) == -1) {
            $scope.selected_colors.push(new_color);
            $scope.tee.colors = $scope.selected_colors;
        }
    };
    
    $scope.editPhoto = function(){
        setTimeout(function () {
            angular.element('.tee-upload').click();
            angular.element('.tee-upload').change(function (e) {
                var input = e.target;
                loadImage(input.files);
            });
        }, 10);
    };
    
    $scope.editSave = function(tee){
//        console.log(tee);
//        tee.error = {};
//        
//        if($scope.photo)
//            tee.error.image = $scope.photo;
//        
//        console.log($scope.tee);
//        console.log('Am here!!');
//        return;
//        
        console.log(tee);
        ShirtServices.updatePost($scope.tee,function(tee){
            console.log(tee);
        });
    };
    
    var loadImage = function (file) {
        file = file[0];
        var reader = new FileReader();
        if (file) {
           
            reader.onload = function () {
                var image = new Image();
                image.src = reader.result;
                var data = reader.result;
                
                if(image.width != 800 && image.height != 400){                    
                    $scope.photo = " Image dimension should be 800 X 400";                   
                }
                
                data = data.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
              
                $scope.tee.image = data;
                var img = $("<img/>").attr('class', 'img-responsive').attr("src", "data:image/gif;base64," + data);
                
                angular.element('.image_preview').html(img);
            };

            reader.readAsDataURL(file);
        }
    };

});