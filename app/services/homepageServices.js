var homepageServices = angular.module('homepageServices',[]);

homepageServices.service('ShirtServices', function($http){
    
    this.Types = function(callback){
        var jsonurl = 'app/api/tee_defaults.json';
        $http({
            url     :   jsonurl,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    };

    this.Colors = function(callback){
        var jsonurl = 'app/api/tee_colors.json';
        $http({
            url     :   jsonurl,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    };

    this.Filters = function(callback){
        var jsonurl = 'app/api/tee_filters.json';
        $http({
            url     :   jsonurl,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    };

    this.Post = function(tee,brand,callback){
      
        var post = {
            "error" : 0
        };
        console.log(tee);
        console.log(brand);
        
        callback(post);
    };
    
    this.updatePost = function(tee, callback){
        var post = {
            "error" : 0
        };
        
        callback(post);
    };

    this.Shirts = function(filters, callback){
      
        var filterstring = $.param(filters);
        var jsonurl = 'app/api/tee_shirts.json?'+filterstring;
      
//        console.log(jsonurl);
        $http({
            url     :   jsonurl,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    };

    this.Shirt = function(id, callback){
        var jsonurl = 'app/api/tee_shirt.json?reqid='+new Date().getTime();
        $http({
            url     :   jsonurl,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    };

    this.ShirtShare = function(url,id,callback){
        $http({
            url     :   url+"?id="+id,
            method  :   'GET'
        }).then(function(response){
            callback(response.data.data);
        });
    }

});

homepageServices.service('AuthServices', function($http, $localStorage){

    var token       =  ($localStorage.token) ? $localStorage.token : '';
    var apiBaseUrl  =  'http://api.teeshirts.ph:8000';
//    var apiBaseUrl  =  'http://192.168.22.5:8000';
    var current     =  this;

    this.Login = function(email, password, callback){
        var data    =   {
            email       :   email,
            password    :   password
        };
        
        data = $.param(data);
        $http({
            url     :   apiBaseUrl + '/login',
            method  :   'POST',
            data    :   data,
            headers :   {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function(response){
            var token   =   (response.data.data)?response.data.data.token:null;
            callback(response.data.error,token);
        });
    };

    this.getAccount = function(callback){
        
        $http({
            url     :   apiBaseUrl+'/me?token='+token,
            method  :   'GET'
        }).then(function(response){
            callback(response.data);
        }).catch(function(){
            current.getAccount(callback);
        });
    };

});

homepageServices.service('DahboardServices', function($http, $localStorage){
    
    this.updateProfile = function(profile, callback){
        var post = {
            "error" : 0
        };
        
        callback(post);
        
    };
    
    
});
